<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\NodeJs;

use BadPixxel\Robo\NodeJs\Robo\Plugin\Tasks\NodeJs;
use Robo\Collection\CollectionBuilder;
use Robo\Exception\TaskException;
use Robo\Result;

/**
 * Trait for Using Node Js Tasks
 */
trait NodeJsTasksTrait
{
    /**
     * Install Node Js via APT Task
     *
     * @param null|string $version
     *
     * @return CollectionBuilder|NodeJs\AptInstall
     */
    protected function taskNodeJsAptInstall(string $version = null)
    {
        return $this->task(NodeJs\AptInstall::class, $version);
    }

    /**
     * Automated Node Js Install
     *
     * @throws TaskException
     *
     * @return Result
     */
    protected function autoConfigNodeJs(): Result
    {
        /** @var NodeJs\AptInstall $task */
        $task = $this->taskNodeJsAptInstall();

        return $task
            ->detectVersion()
            ->setRequired(false)
            ->run()
        ;
    }
}
