<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\NodeJs;

use BadPixxel\Robo\NodeJs\Robo\Plugin\Tasks\Yarn;
use Robo\Collection\CollectionBuilder;
use Robo\Exception\TaskException;
use Robo\Result;

/**
 * Trait for Using Yarn Tasks
 */
trait YarnTasksTrait
{
    /**
     * Install Yarn via APT Task
     *
     * @return CollectionBuilder|Yarn\AptInstall
     */
    protected function taskYarnAptInstall()
    {
        return $this->task(Yarn\AptInstall::class);
    }

    /**
     * Automated Yarn Install
     *
     * @throws TaskException
     *
     * @return Result
     */
    protected function autoConfigYarn(): Result
    {
        /** @var Yarn\AptInstall $task */
        $task = $this->taskYarnAptInstall();

        return empty(getenv("YARN_VERSION"))
            ? Result::success($task)
            : $task->run()
        ;
    }

    /**
     * Get Yarn Docker Install Task
     *
     * @return CollectionBuilder|Yarn\Install
     */
    protected function taskYarnInstall()
    {
        return $this->task(Yarn\Install::class);
    }

    /**
     * Get Yarn Docker Upgrade Task
     *
     * @return CollectionBuilder|Yarn\Upgrade
     */
    protected function taskYarnUpgrade()
    {
        return $this->task(Yarn\Upgrade::class);
    }
}
