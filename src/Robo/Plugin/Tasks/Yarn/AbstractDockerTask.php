<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\NodeJs\Robo\Plugin\Tasks\Yarn;

use Robo\Contract\BuilderAwareInterface;
use Robo\LoadAllTasks;
use Robo\Result;
use Robo\Task\BaseTask;
use Robo\Task\Docker\Remove;
use Robo\Task\Docker\Run;

/**
 * Execute Yarn Tasks Dependencies via Docker
 */
abstract class AbstractDockerTask extends BaseTask implements BuilderAwareInterface
{
    use LoadAllTasks;

    /**
     * Path to Sources
     *
     * @var string
     */
    private string $sources;

    /**
     * Node Docker Image to Use
     *
     * @var string
     */
    private string $image = "node:16-alpine";

    /**
     * Node Docker Container Name
     *
     * @var string
     */
    private string $name = "badpixxel-node";

    /**
     * @inheritDoc
     */
    public function run(): Result
    {
        //====================================================================//
        // Safety Check
        if (empty($this->sources) || empty(realpath($this->sources))) {
            return Result::error($this, "No Yarn Sources Path Defined");
        }
        //====================================================================//
        // Remove Any Existing Docker Container
        /** @var Remove $dockerRemove */
        $dockerRemove = $this->taskDockerRemove($this->name);
        $dockerRemove->arg('-f')->run();
        //====================================================================//
        // Execute Yarn Command via Docker
        /** @var Run $dockerRun */
        $dockerRun = $this->taskDockerRun($this->image);
        $result = $dockerRun
            ->containerWorkdir("/srv")
            ->detached()
            ->name($this->name)
            ->volume(realpath($this->sources), "/srv:Z")
            ->exec($this->getCommand())
            ->run()
        ;
        //====================================================================//
        // Return Action Result
        return $result->wasSuccessFul()
            ? Result::success($this, "Yarn Command Done")
            : Result::error($this, "Yarn Command Fail")
        ;
    }

    /**
     * Set Sources Path
     *
     * @param string $sources
     *
     * @return $this
     */
    public function sources(string $sources): self
    {
        $this->sources = $sources;

        return $this;
    }

    /**
     * Set Docker Image to Use
     *
     * @param string $image
     *
     * @return $this
     */
    public function image(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get Yarn Command to Execute
     *
     * @return string
     */
    abstract protected function getCommand(): string;
}
