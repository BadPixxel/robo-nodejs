<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\NodeJs\Robo\Plugin\Tasks\Yarn;

/**
 * Upgrade Yarn Dependencies via Docker
 */
class Upgrade extends AbstractDockerTask
{
    /**
     * Path to Vendor
     *
     * @var string
     */
    private string $target = "node_modules";

    /**
     * Set Path to Vendor
     *
     * @param string $target
     *
     * @return $this
     */
    public function target(string $target): self
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get Command To Execute
     *
     * @return string
     */
    public function getCommand(): string
    {
        return sprintf('yarn upgrade --modules-folder=/src%s', $this->target);
    }
}
