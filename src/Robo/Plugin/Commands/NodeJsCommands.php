<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\NodeJs\Robo\Plugin\Commands;

use BadPixxel\Robo\NodeJs\NodeJsTasksTrait;
use BadPixxel\Robo\NodeJs\Robo\Plugin\Tasks\NodeJs;
use Robo\Exception\TaskException;
use Robo\Symfony\ConsoleIO;
use Robo\Tasks;

/**
 * Robo Commands to Install Applications
 */
class NodeJsCommands extends Tasks
{
    use NodeJsTasksTrait;

    /**
     * @command add:node-js
     *
     * @description Install Node JS
     *
     * @param null|string $version
     *
     * @throws TaskException
     *
     * @return int
     */
    public function installNodeJs(ConsoleIO $consoleIo, string $version = null): int
    {
        /** @var NodeJs\AptInstall $task */
        $task = $this->taskNodeJsAptInstall($version);

        $result = $task
            ->detectVersion()
            ->askForVersion($consoleIo)
            ->run()
        ;
        $result->wasSuccessful()
            ? $consoleIo->success($result->getMessage())
            : $consoleIo->error($result->getMessage())
        ;

        return $result->getExitCode();
    }
}
