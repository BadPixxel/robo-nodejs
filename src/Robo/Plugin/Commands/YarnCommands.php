<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\NodeJs\Robo\Plugin\Commands;

use BadPixxel\Robo\NodeJs\Robo\Plugin\Tasks\Yarn;
use BadPixxel\Robo\NodeJs\YarnTasksTrait;
use Robo\Exception\TaskException;
use Robo\Symfony\ConsoleIO;
use Robo\Tasks;

/**
 * Robo Commands to Install Applications
 */
class YarnCommands extends Tasks
{
    use YarnTasksTrait;

    /**
     * @command add:yarn
     *
     * @description Install Yarn Package Manager
     *
     * @throws TaskException
     *
     * @return int
     */
    public function installYarn(ConsoleIO $consoleIo): int
    {
        /** @var Yarn\AptInstall $task */
        $task = $this->taskYarnAptInstall();

        $result = $task->run();
        $result->wasSuccessful()
            ? $consoleIo->success($result->getMessage())
            : $consoleIo->error($result->getMessage())
        ;

        return $result->getExitCode();
    }
}
